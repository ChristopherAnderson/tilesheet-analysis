# Analysis of sprite usage 

Determines often or less often picked tiles for a given tilesheet, in [Playscii](http://vectorpoem.com/playscii/)'s bitmap import. Possibly a more general import and analysis will be developed. (but Playscii is really cool and you should try it!)

As a short example, you have made a tilesheet and use it to import a number of images, which you then save as `.psci` files. You would
like to tune your tilesheet to be more effective at this task, so you determine which sprites are outliers and if they underperform, 
you may want to exchange them for one similar to an overperforming sprite. 

See TODO: example, for examples.

# Generation of tilesheets

This will generate tiles into a tilesheet, probably according to a few parameters.
